unit testobject;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, memento_interfaces, memento_objects, strutils;

const
  cs = ['{','}'];
type

 { TTestObject }

  TTestObject = class(TInterfacedObject, IOriginator)
  private
    fA: integer;
    fB: integer;
    function GetText: string;
  public
    property A: integer read fA write fA;
    property B: integer read fB write fB;
    property Text: string read GetText;
    function Save(_hint: string): IMemento;
  end;

   { TObjectMemento }

  TObjectMemento = class(TAbstractMemento)
  private
    fA: integer;
    fB: integer;
    fHint: string;
    fObject: TTestObject;
    fTime: TDateTime;
  public
    property A: integer read fA;
    property B: integer read fB;
    property Hint: string read fHint;
    property Time: TDateTime read fTime;
    procedure Restore; override;
    function toStr: string; override;
    constructor Create(originator: TTestObject; _hint: string);
  end;

  TObjectsCaretaker = class(TAbstractCaretaker)
  end;

implementation

{ TObjectMemento }

procedure TObjectMemento.Restore;
begin
  fObject.A := A;
  fObject.B := B;
end;

function TObjectMemento.toStr: string;
begin
  Result := TimeToStr(Time) + ' '+Hint+ ' A=' + IntToStr(A) + ' B=' + IntToStr(B);
end;

constructor TObjectMemento.Create(originator: TTestObject; _hint: string);
begin
  fObject := originator;
  fA := originator.A;
  fB := originator.B;
  fHint := _hint;
  fTime := Now();
end;

{ TTestObject }

function TTestObject.GetText: string;
var g: TGuid;
begin
  {Result := 'A + B = ' + IntToStr(A + B) + #13#10;
  Result += 'A - B = ' + IntToStr(A - B) + #13#10;
  if (A > B) then Result += 'A>B'
    else if (A < B) then Result += 'A<B'
      else Result += 'A=B';  }
  CreateGuid(g);
  Result:=TrimSet(GuidToString(g),cs);
end;

function TTestObject.Save(_hint: string): IMemento;
begin
  Result := TObjectMemento.Create(Self, _hint);
end;

end.
