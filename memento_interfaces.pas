unit memento_interfaces;

{$mode objfpc}{$H+}

interface

uses
  Classes, fgl;

type

  { IMemento }

  IMemento = interface
    ['{6BCD1CFA-B771-42F9-9C7A-1F1ECE1AAA3D}']
    procedure Restore;
    function toStr: string;
  end;

  TMementoList = specialize TFPGInterfacedObjectList<IMemento>;

  { IOriginator }

  IOriginator = interface
    ['{2475CE17-FD74-4ECB-8BB8-2C1B47185288}']
    function Save(_hint: string): IMemento;
  end;

  { ICaretaker }

  ICaretaker = interface
    ['{E2FE0CBC-8336-441D-BABA-70A45A86D8C5}']
    function GetmementoList: TMementoList;
    function GetMementoListStr: string;
    procedure Restore(index: integer);
    procedure Add(_memento: IMemento);
  end;

implementation
end.

