unit memento_objects;

{$mode objfpc}{$H+}

interface

uses
  Classes, memento_interfaces;

type

  { TAbstractMemento }

  TAbstractMemento = class(TInterfacedObject, IMemento)
  public
    procedure Restore; virtual; abstract;
    function toStr: string; virtual; abstract;
  end;

  { TAbstractCaretaker }

  TAbstractCaretaker = class(TInterfacedObject, ICaretaker)
  protected
    fMementoList: TMementoList;
  public
    function GetMementoListStr: string;
    function GetmementoList: TMementoList;
    procedure Restore(index: integer);
    procedure Add(_memento: IMemento);
    destructor Destroy; override;
  end;

implementation

{ TAbstractCaretaker }

function TAbstractCaretaker.GetMementoListStr: string;
var
  i: integer;
begin
  Result := '';
  if fMementoList = nil then
    exit;
  for i := 0 to fMementoList.Count - 1 do
    Result += fMementoList[i].toStr+#13#10;
end;

function TAbstractCaretaker.GetmementoList: TMementoList;
var
  i: integer;
begin
  if fMementoList = nil then exit;
  Result := TMementoList.Create;
  for i := 0 to fMementoList.Count - 1 do
    Result.Add(fMementoList[i]);
end;

procedure TAbstractCaretaker.Restore(index: integer);
begin
  fMementoList[index].Restore;
  fMementoList.Delete(index);
end;

procedure TAbstractCaretaker.Add(_memento: IMemento);
begin
  if fMementoList = nil then
    fMementoList := TMementoList.Create;
  fMementoList.Add(_memento);
end;

destructor TAbstractCaretaker.Destroy;
begin
  fmementoList.Free;
end;

end.

