unit maintest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, testobject;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ListBox1: TListBox;
    Memo1: TMemo;
    UpDown1: TUpDown;
    UpDown2: TUpDown;
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    Obj1: TTestObject;
    ObjList: TObjectsCaretaker;
    procedure RefreshMemos;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
   Obj1 := TTestObject.Create;
   ObjList := TObjectsCaretaker.Create;
   Obj1.A:=StrToInt(Edit1.Text);
   Obj1.B:=StrToInt(Edit2.Text);
   RefreshMemos;
end;

procedure TForm1.RefreshMemos;
begin
  Memo1.Lines.Text:=Obj1.Text;
  ListBox1.Items.Clear;
  ListBox1.Items.Text:= ObjList.GetMementoListStr;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  ObjList.Free;
  Obj1.Free;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
   Obj1.A:=StrToInt(Edit1.Text);
   ObjList.Add(Obj1.Save('изменено поле А'));
   RefreshMemos;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex<0 then exit;
  ObjList.Restore(ListBox1.ItemIndex);
  Label4.Caption:='восстановлено состояние номер '+IntToStr(ListBox1.ItemIndex+1);
  Edit1.Text:=IntToStr(Obj1.A);
  Edit2.Text:=IntToStr(Obj1.B);
  RefreshMemos;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
   Obj1.B:=StrToInt(Edit2.Text);
   ObjList.Add(Obj1.Save('изменено поле Б'));
   RefreshMemos;
end;

end.

